package com.yossefdouieb.WiFocus;

import android.graphics.drawable.Drawable;

class MyData {

    private String id;
    private String title;
    private Drawable AppIcon;

    public MyData(String id, String title, Drawable _AppIcon) {
        this.id = id;
        this.title = title;
        this.AppIcon = _AppIcon;
    }


    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Drawable getAppIcon() {
        return this.AppIcon;
    }

}
