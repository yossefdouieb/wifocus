package com.yossefdouieb.WiFocus;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.telecom.TelecomManager;
import android.telephony.TelephonyManager;

import com.android.internal.telephony.ITelephony;

import java.lang.reflect.Method;

public class IncomingCallReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        SharedPreferences pref = context.getSharedPreferences("MyPref", 0);

        if (pref.getBoolean("BlockCalls", false)) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                TelecomManager tm = (TelecomManager) context.getSystemService(Context.TELECOM_SERVICE);
                if (tm == null) {
                    throw new NullPointerException("tm == null");
                }
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ANSWER_PHONE_CALLS) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                tm.endCall();

            } else {
                ITelephony telephonyService;
                try {
                    String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
                    String number = intent.getExtras().getString(TelephonyManager.EXTRA_INCOMING_NUMBER);

                    if (state.equalsIgnoreCase(TelephonyManager.EXTRA_STATE_RINGING)) {
                        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                        try {
                            Method m = tm.getClass().getDeclaredMethod("getITelephony");

                            m.setAccessible(true);
                            telephonyService = (ITelephony) m.invoke(tm);

                            if ((number != null)) {
                                telephonyService.endCall();
                                //Toast.makeText(context, "Ending the call from: " + number, Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        //Toast.makeText(context, "Ring " + number, Toast.LENGTH_SHORT).show();

                    }
//                    if (state.equalsIgnoreCase(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
//                        //Toast.makeText(context, "Answered " + number, Toast.LENGTH_SHORT).show();
//                    }
//                    if (state.equalsIgnoreCase(TelephonyManager.EXTRA_STATE_IDLE)) {
//                        //Toast.makeText(context, "Idle "+ number, Toast.LENGTH_SHORT).show();
//                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
