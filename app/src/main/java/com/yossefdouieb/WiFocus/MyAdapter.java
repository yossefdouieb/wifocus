package com.yossefdouieb.WiFocus;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private final Context context;
    private final List<MyData> list;
    private List<String> selectedIds = new ArrayList<>();

    public MyAdapter(Context context, List<MyData> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.adapter_item_layout, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.title.setText(list.get(position).getTitle());
        String id = list.get(position).getId();
        holder.appIcon.setImageDrawable(list.get(position).getAppIcon());

        if (selectedIds.contains(id)) {
            //if item is selected then,set foreground color of FrameLayout.
            holder.rootView.setBackground(new ColorDrawable(ContextCompat.getColor(context, R.color.colorPrimary)));
        } else {
            //else remove selected item color.
            holder.rootView.setBackground(new ColorDrawable(ContextCompat.getColor(context, android.R.color.transparent)));
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public MyData getItem(int position) {
        return list.get(position);
    }

    public void setSelectedIds(List<String> selectedIds) {
        this.selectedIds = selectedIds;
        notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        final TextView title;
        final FrameLayout rootView;
        final ImageView appIcon;

        MyViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            appIcon = itemView.findViewById(R.id.app_icon);
            rootView = itemView.findViewById(R.id.root_view);
        }
    }

}