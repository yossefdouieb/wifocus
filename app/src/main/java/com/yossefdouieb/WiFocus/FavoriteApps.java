package com.yossefdouieb.WiFocus;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.Collections;

public class FavoriteApps extends AppCompatActivity {

    private final ArrayList<String> stringArrayList = new ArrayList<>();
    private RecyclerView recyclerView;
    private RecyclerViewAdapter mAdapter;
    private CoordinatorLayout relativeLayout;

    private String[] ItemsArr;

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    private String NewArr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favortie_apps);

        Toolbar toolbar = findViewById(R.id.FavoriteToolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        setTitle(getApplicationContext().getString(R.string.favorite_apps_list));

        pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        editor = pref.edit();

        NewArr = pref.getString("FavoriteApps", "");

        //top banner
        AdView mAdView = findViewById(R.id.FavoriteAppsBanner);
        mAdView.loadAd(new AdRequest.Builder().addTestDevice("6D94567FA64A4AF46ECF162C32028F78").build());

        recyclerView = findViewById(R.id.FavoriteAppsRecyclerView);
        relativeLayout = findViewById(R.id.FavoriteLayout);

        populateRecyclerView();
        enableSwipeToDeleteAndUndo();

        FloatingActionButton AddAppsButton = findViewById(R.id.AddAppsButton);
        AddAppsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent SelectApps = new Intent(getApplicationContext(), SelectAppsActivity.class);
                SelectApps.putExtra("AddToFavorite", true);
                startActivity(SelectApps);
            }
        });

        ImageButton ButtonGo = findViewById(R.id.ButtonGo);
        ButtonGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringBuilder SelectedArr = new StringBuilder();
                for (RecyclerViewAdapter.Model model : mAdapter.mModelList) {
                    if (model.isSelected()) {
                        SelectedArr.append(model.getText());
                        SelectedArr.append(",");
                    }
                }
                String NotNullCheck = MainActivity.removeLastChar(SelectedArr.toString());
                if (NotNullCheck != null) {
                    SelectedArr = new StringBuilder(NotNullCheck);
                    if (SelectedArr.toString().equals("")) {
                        Toast.makeText(getApplicationContext(), getApplicationContext().getString(R.string.select_apps_first), Toast.LENGTH_LONG).show();
                    } else {
                        editor.putString("PackagesList", SelectedArr.toString());
                        editor.apply();
                        Intent BackToMain = new Intent(getApplicationContext(), MainActivity.class);
                        BackToMain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(BackToMain);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), getApplicationContext().getString(R.string.select_apps_first), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void populateRecyclerView() {
        stringArrayList.clear();
        String StringArray = pref.getString("FavoriteApps", "");
        ItemsArr = StringArray.split(",");
        ((TextView) findViewById(R.id.TextView)).setText(getApplicationContext().getString(R.string.favorite_list_empty));
        if (!ItemsArr[0].equals("")) {
            ((TextView) findViewById(R.id.TextView)).setText(getApplicationContext().getString(R.string.delete_from_favorite));
            Collections.addAll(stringArrayList, ItemsArr);
        }

        mAdapter = new RecyclerViewAdapter(getApplicationContext(), stringArrayList);
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    protected void onResume() {
        populateRecyclerView();
        NewArr = pref.getString("FavoriteApps", "");
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    private void enableSwipeToDeleteAndUndo() {
        SwipeToDeleteCallback swipeToDeleteCallback = new SwipeToDeleteCallback(this) {
            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

                final int position = viewHolder.getAdapterPosition();
                final String item = mAdapter.getData().get(position);

                mAdapter.removeItem(position);

                NewArr = MainActivity.removeFromStringArr(NewArr, ItemsArr[position]);
                Snackbar snackbar = Snackbar.make(relativeLayout, getApplicationContext().getString(R.string.app_removed), Snackbar.LENGTH_LONG);
                snackbar.setAction(getApplicationContext().getString(R.string.undo), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mAdapter.restoreItem(item, position);
                        recyclerView.scrollToPosition(position);
                        NewArr += "," + item;
                        NewArr = CleanString(NewArr);
                        editor.putString("FavoriteApps", NewArr);
                        editor.apply();
                        populateRecyclerView();
                    }
                });

                snackbar.setActionTextColor(Color.YELLOW);
                snackbar.show();
                editor.putString("FavoriteApps", NewArr);
                editor.apply();
                populateRecyclerView();
            }
        };

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeToDeleteCallback);
        itemTouchhelper.attachToRecyclerView(recyclerView);
    }

    private String CleanString(String Arr) {
        if (Arr.charAt(0) == ',') {
            Arr = Arr.substring(1);
        }
        if (Arr.charAt(Arr.length() - 1) == ',') {
            Arr = MainActivity.removeLastChar(Arr);
        }
        return Arr;
    }

    @Override
    protected void onDestroy() {
        editor.putString("FavoriteApps", NewArr);
        editor.apply();
        super.onDestroy();
    }
}
