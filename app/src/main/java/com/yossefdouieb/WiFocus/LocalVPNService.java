package com.yossefdouieb.WiFocus;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.VpnService;
import android.os.ParcelFileDescriptor;
import android.support.v4.content.LocalBroadcastManager;

public class LocalVPNService extends VpnService {
    public static final String BROADCAST_VPN_STATE = "com.yossefdouieb.WiFocus.VPN_STATE";
    private static final String TAG = LocalVPNService.class.getSimpleName();
    private static final String VPN_ADDRESS = "10.0.0.1"; // Only IPv4 support for now
    private static final String VPN_ROUTE = "0.0.0.0"; // Intercept everything
    private static boolean isRunning = false;

    private ParcelFileDescriptor vpnInterface = null;

    private SharedPreferences pref;

    private final BroadcastReceiver stopBr = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if ("stop_kill".equals(intent.getAction())) {
                StopMyVPN();
            }
        }
    };

    private PendingIntent pendingIntent;

    public static boolean isRunning() {
        return isRunning;
    }

    private void StopMyVPN() {
        try {
            if (vpnInterface != null) {
                vpnInterface.close();
            }
            isRunning = false;
        } catch (Exception e) {
            //Log.e("StopVpn", e.getMessage());
        }
        stopSelf();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        isRunning = true;

        pref = getApplicationContext().getSharedPreferences("MyPref", 0);

        LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(this);
        lbm.registerReceiver(stopBr, new IntentFilter("stop_kill"));


        setupVPN();
        try {
            LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(BROADCAST_VPN_STATE).putExtra("running", true));
            //Log.i(TAG, "Started");
        } catch (Exception e) {
            //Log.e(TAG, "Error starting service", e);
        }
    }

    private void setupVPN() {
        if (vpnInterface == null) {
            Builder builder = new Builder();

            try {
                //VPN is not connected to the internet, disallowed app traffic will go through the normal connection
                String PackagesList = pref.getString("PackagesList", "");
                if (!PackagesList.equals("")) {
                    String[] PackagesArr = PackagesList.split(",");
                    for (String CurrPackage : PackagesArr) {
                        builder.addDisallowedApplication(CurrPackage);
                    }
                    //add the app and Google play services to focus list, without internet connection the app cant load ads...
                    builder.addDisallowedApplication("com.yossefdouieb.WiFocus");
                    builder.addDisallowedApplication("com.google.android.gms");
                }

            } catch (PackageManager.NameNotFoundException e) {
                //Log.d("DisallowedApplication", e.getMessage());
                e.printStackTrace();
            }

            builder.addAddress(VPN_ADDRESS, 32);
            builder.addRoute(VPN_ROUTE, 0);
            vpnInterface = builder.setSession(getString(R.string.app_name)).setConfigureIntent(pendingIntent).establish();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isRunning = false;
        //Log.i(TAG, "Stopped");
    }
}