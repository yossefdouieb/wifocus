package com.yossefdouieb.WiFocus;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SelectAppsActivity extends AppCompatActivity implements ActionMode.Callback {
    private boolean AddSelected;
    private RecyclerView recyclerView;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private ActionMode actionMode;
    private boolean isMultiSelect = false;
    private List<String> selectedIds = new ArrayList<>();
    private PackageManager packageManager = null;
    private List<MyData> MyDataList;
    private MyAdapter adapter = null;

    private static String removeLastChar(String s) {
        return (s == null || s.length() == 0) ? null : (s.substring(0, s.length() - 1));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_apps);

        AddSelected = getIntent().getBooleanExtra("AddSelected", false);

        if (AddSelected) {
            setTitle(getApplicationContext().getString(R.string.add_apps_title));
        } else {
            setTitle(getApplicationContext().getString(R.string.select_apps_title));
        }
        //top banner
        AdView mAdView = findViewById(R.id.adView);
        mAdView.loadAd(new AdRequest.Builder().addTestDevice("6D94567FA64A4AF46ECF162C32028F78").build());

        Toolbar toolbar = findViewById(R.id.SelectAppsToolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        packageManager = getPackageManager();

        recyclerView = findViewById(R.id.widget_list);
        new LoadApplications().execute();

        pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        editor = pref.edit();

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (isMultiSelect) {
                    //if multiple selection is enabled then select item on single click else perform normal click on item.
                    multiSelect(position);
                }

                if (!isMultiSelect) {
                    selectedIds = new ArrayList<>();
                    isMultiSelect = true;

                    if (actionMode == null) {
                        actionMode = startActionMode(SelectAppsActivity.this); //show ActionMode.
                    }
                    multiSelect(position);
                }

                if (selectedIds.isEmpty()) {
                    isMultiSelect = false;
                }
            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));
    }

    private void multiSelect(int position) {
        MyData data = adapter.getItem(position);
        if (data != null) {
            if (actionMode != null) {
                if (selectedIds.contains(data.getId()))
                    selectedIds.remove(data.getId());
                else
                    selectedIds.add(data.getId());

                if (selectedIds.size() > 0)
                    actionMode.setTitle(String.valueOf(selectedIds.size()) + getApplicationContext().getString(R.string.apps_selected)); //show selected item count on action mode.
                else {
                    actionMode.setTitle(""); //remove item count from action mode.
                    actionMode.finish(); //hide action mode.
                }
                adapter.setSelectedIds(selectedIds);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.menu_select, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.action_done:
                StringBuilder SelectedAppsPackages = new StringBuilder();
                //if add button is pressed we want only to add the selected apps to the list and not to delete the previous selected apps
                if (AddSelected) {
                    //getting the "old" list
                    SelectedAppsPackages = new StringBuilder(pref.getString("PackagesList", ""));
                    SelectedAppsPackages.append(",");
                }

                if (getIntent().getBooleanExtra("AddToFavorite", false)) {
                    SelectedAppsPackages = new StringBuilder(pref.getString("FavoriteApps", ""));
                    if (!SelectedAppsPackages.toString().equals("")) {
                        SelectedAppsPackages.append(",");
                    }
                }

                for (MyData data : MyDataList) {
                    if (selectedIds.contains(data.getId())) {
                        SelectedAppsPackages.append(data.getId()).append(",");
                    }
                }
                SelectedAppsPackages = new StringBuilder(removeLastChar(SelectedAppsPackages.toString()));

                if (getIntent().getBooleanExtra("AddToFavorite", false)) {
                    editor.putString("FavoriteApps", SelectedAppsPackages.toString());
                    editor.apply();

                    onBackPressed();
                } else {
                    //Now, reopening main activity with the selected apps
                    editor.putString("PackagesList", SelectedAppsPackages.toString());
                    editor.apply();


                    Intent BackToMain = new Intent(this, MainActivity.class);
                    BackToMain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(BackToMain);
                }

                return true;

        }
        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        actionMode = null;
        selectedIds = new ArrayList<>();
        adapter.setSelectedIds(new ArrayList<String>());
    }

    private List<ApplicationInfo> checkForLaunchIntent(List<ApplicationInfo> list) {
        ArrayList<ApplicationInfo> appList = new ArrayList<>();
        for (ApplicationInfo info : list) {
            try {
                if (null != packageManager.getLaunchIntentForPackage(info.packageName)) {
                    appList.add(info);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return appList;
    }


    private class LoadApplications extends AsyncTask<Void, Void, Void> {
        private ProgressDialog progress = null;

        @Override
        protected Void doInBackground(Void... params) {
            List<ApplicationInfo> appList = checkForLaunchIntent(packageManager.getInstalledApplications(PackageManager.GET_META_DATA));

            MyDataList = new ArrayList<>();
            if (getIntent().getBooleanExtra("AddToFavorite", false)) {
                for (ApplicationInfo applicationInfo : appList) {
                    if (!pref.getString("FavoriteApps", "").contains(applicationInfo.packageName)) {
                        MyData current = new MyData(applicationInfo.packageName, applicationInfo.loadLabel(packageManager).toString(), applicationInfo.loadIcon(packageManager));
                        MyDataList.add(current);
                    }
                }
            } else {
                if (AddSelected) {
                    for (ApplicationInfo applicationInfo : appList) {
                        if (!pref.getString("PackagesList", "").contains(applicationInfo.packageName)) {
                            MyData current = new MyData(applicationInfo.packageName, applicationInfo.loadLabel(packageManager).toString(), applicationInfo.loadIcon(packageManager));
                            MyDataList.add(current);
                        }
                    }
                } else {
                    for (ApplicationInfo applicationInfo : appList) {
                        MyData current = new MyData(applicationInfo.packageName, applicationInfo.loadLabel(packageManager).toString(), applicationInfo.loadIcon(packageManager));
                        MyDataList.add(current);
                    }
                }
            }

            Collections.sort(MyDataList, new CustomComparator());
            adapter = new MyAdapter(getApplicationContext(), MyDataList);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            recyclerView.setAdapter(adapter);
            progress.dismiss();
            super.onPostExecute(result);
        }

        @Override
        protected void onPreExecute() {
            progress = ProgressDialog.show(SelectAppsActivity.this, null, getApplicationContext().getString(R.string.loading_apps_info));
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

    }

    class CustomComparator implements Comparator<MyData> {
        @Override
        public int compare(MyData o1, MyData o2) {
            return o1.getTitle().compareTo(o2.getTitle());
        }
    }
}
