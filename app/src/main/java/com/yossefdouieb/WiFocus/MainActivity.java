package com.yossefdouieb.WiFocus;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.VpnService;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import java.net.InetAddress;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;

import pl.bclogic.pulsator4droid.library.PulsatorLayout;

public class MainActivity extends AppCompatActivity {
    private static final int VPN_REQUEST_CODE = 0x0F;
    private final int PERMISSION_REQUEST_READ_PHONE_STATE = 23;
    private boolean InternetIsAvailable = false;
    private Animation FadeOut;
    private PulsatorLayout pulsator;
    private boolean waitingForVPNStart;
    private final BroadcastReceiver vpnStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (LocalVPNService.BROADCAST_VPN_STATE.equals(intent.getAction())) {
                if (intent.getBooleanExtra("running", false))
                    waitingForVPNStart = false;
            }
        }
    };
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private InterstitialAd mInterstitialAd;
    private Boolean VpnStarted = false;
    private boolean BlockCalls = false;
    private Button SelectAppsButton;
    private String[] SelectedAppsPackages;
    private Button vpnButton;
    private Switch BlockCallsSwitch;

    public static String removeLastChar(String s) {
        return (s == null || s.length() == 0) ? null : (s.substring(0, s.length() - 1));
    }

    public static String removeFromStringArr(String oldArr, String toRemove) {
        String NewString = oldArr.replace(toRemove, "");
        NewString = NewString.replace(",,", ",");
        if (!NewString.equals("")) {
            if (NewString.charAt(0) == ',') {
                NewString = NewString.substring(1);
            }
            if (NewString.charAt(NewString.length() - 1) == ',') {
                NewString = removeLastChar(NewString);
            }
        }
        return NewString;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        editor = pref.edit();

        if (pref.getBoolean("FirstUse", true)) {
            Intent ShowTutorial = new Intent(getApplicationContext(), TutorialActivity.class);
            startActivity(ShowTutorial);
        }

        Toolbar mTopToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(mTopToolbar);

        pulsator = findViewById(R.id.pulsator);

        editor.putBoolean("BlockCalls", false);
        editor.apply();

        CheckInternetConnection();
        //initialize adMob
        MobileAds.initialize(this, "ca-app-pub-7960596790811619~8037033587");

        //selected apps list (horizontalScrollView)
        FadeOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out);
        InflateAppsList();

        //top banner
        AdView mAdView = findViewById(R.id.MainAdView);
        mAdView.loadAd(new AdRequest.Builder().addTestDevice("6D94567FA64A4AF46ECF162C32028F78").build());

        //start button OnClick ad
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-7960596790811619/6695019996");
        final AdRequest request = new AdRequest.Builder()
                .addTestDevice("6D94567FA64A4AF46ECF162C32028F78")
                .build();
        mInterstitialAd.loadAd(request);
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                // Load the next ad.
                mInterstitialAd.loadAd(request);
            }

            @Override
            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                //Log.e("adFailedToLoad", String.valueOf(i));
                mInterstitialAd.loadAd(request);
            }
        });

        SelectAppsButton = findViewById(R.id.SelectAppsButton);
        SelectAppsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent AppList = new Intent(getApplicationContext(), SelectAppsActivity.class);
                startActivity(AppList);
            }
        });

        vpnButton = findViewById(R.id.StartVpnButton);
        vpnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (VpnStarted) {
                    StopVpn();
                } else {
                    if (SelectedAppsPackages[0].equals("")) {
                        FlickerButtonText();
                        Toast.makeText(getApplicationContext(), getApplicationContext().getString(R.string.select_apps_first), Toast.LENGTH_LONG).show();
                        return;
                    }
                    if (mInterstitialAd.isLoaded()) {
                        mInterstitialAd.show();
                    }

                    CheckInternetConnection();
                    if (InternetIsAvailable) {
                        startVPN();
                    } else {
                        Toast.makeText(getApplicationContext(), getApplicationContext().getString(R.string.need_internet_connection), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        BlockCallsSwitch = findViewById(R.id.BlockCallsSwitch);
        BlockCallsSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //the IncomingCallReceiver Class is reading value from SharedPreferences
                BlockCalls = isChecked;
                if (isChecked == true) {
                    BlockCalls();
                }
                if (VpnStarted) {
                    editor.putBoolean("BlockCalls", BlockCalls);
                    editor.apply();
                }
            }
        });

        waitingForVPNStart = false;
    }

    private String GetDeviceInfo() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        double wi = (double) width / (double) displayMetrics.xdpi;
        double hi = (double) height / (double) displayMetrics.ydpi;
        double x = Math.pow(wi, 2);
        double y = Math.pow(hi, 2);
        double screenInches = Math.sqrt(x + y);

        String info = "SERIAL: " + Build.SERIAL + "\n" +
                "MODEL: " + Build.MODEL + "\n" +
                "ID: " + Build.ID + "\n" +
                "Manufacture: " + Build.MANUFACTURER + "\n" +
                "Brand: " + Build.BRAND + "\n" +
                "Type: " + Build.TYPE + "\n" +
                "User: " + Build.USER + "\n" +
                "BASE: " + Build.VERSION_CODES.BASE + "\n" +
                "INCREMENTAL: " + Build.VERSION.INCREMENTAL + "\n" +
                "SDK:  " + Build.VERSION.SDK_INT + "\n" +
                "BOARD: " + Build.BOARD + "\n" +
                "HARDWARE: " + Build.HARDWARE + "\n" +
                "SCREEN_SIZE_INCHES: " + new DecimalFormat("##.##").format(screenInches) + " Inches" + "\n" +
                "ScreenResolution_value: " + height + " * " + width + " Pixels" + "\n" +
                "SCREEN_DENSITY: " + getResources().getDisplayMetrics().density + "\n" +
                "HOST: " + Build.HOST + "\n" +
                "FINGERPRINT: " + Build.FINGERPRINT + "\n" +
                "Version Code: " + Build.VERSION.RELEASE;

        try {
            info += "\n" + "ANDROID_ID: " + Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return info;
    }

    private String GetMD5Hash(String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest)
                hexString.append(Integer.toHexString(0xFF & aMessageDigest));
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    private void SendBugReportEmail() {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); //only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"douiebyossef@gmail.com"});
        intent.putExtra(Intent.EXTRA_SUBJECT, "Bug Report WiFocus app");
        String DeviceInfoStr = GetDeviceInfo();
        String DeviceInfoPrefixStr = "Write your message here: " + "\n\n" + "__________________________________" + "\n";
        intent.putExtra(Intent.EXTRA_TEXT, DeviceInfoPrefixStr + DeviceInfoStr + "\nHash: " + GetMD5Hash(DeviceInfoStr));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.report_bug: {
                SendBugReportEmail();
                return true;
            }

            case R.id.info: {
                Intent TermsAndConditions = new Intent(getApplicationContext(), TermsAndConditions.class);
                startActivity(TermsAndConditions);
                return true;
            }

            case R.id.favorite_apps: {
                Intent FavoriteAppList = new Intent(getApplicationContext(), FavoriteApps.class);
                startActivity(FavoriteAppList);
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void StopVpn() {
        Intent intent = new Intent("stop_kill");
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
        pulsator.stop();
        editor.putBoolean("BlockCalls", false);
        editor.apply();
        VpnStarted = false;
        waitingForVPNStart = false;
        enableButton(false);
    }

    @Override
    protected void onDestroy() {
        StopVpn();
        super.onDestroy();
    }

    private void FlickerButtonText() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Handler handler2 = new Handler();
                handler2.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        SelectAppsButton.setTextColor(Color.parseColor("#0099cc"));
                    }
                }, 300);
                SelectAppsButton.setTextColor(Color.parseColor("#ff0000"));
            }
        }, 300);
    }

    private void BlockCalls() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_DENIED || checkSelfPermission(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_DENIED || checkSelfPermission(Manifest.permission.ANSWER_PHONE_CALLS) == PackageManager.PERMISSION_DENIED) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    String[] permissions = {Manifest.permission.READ_PHONE_STATE, Manifest.permission.CALL_PHONE, Manifest.permission.ANSWER_PHONE_CALLS};
                    requestPermissions(permissions, PERMISSION_REQUEST_READ_PHONE_STATE);
                } else {
                    String[] permissions = {Manifest.permission.READ_PHONE_STATE, Manifest.permission.CALL_PHONE};
                    requestPermissions(permissions, PERMISSION_REQUEST_READ_PHONE_STATE);
                }
            } else {
                DoNotShowDialog();
            }
        } else {
            DoNotShowDialog();
        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    private boolean isInternetAvailable() {
        try {
            InetAddress ipAddr = InetAddress.getByName("google.com");
            return !ipAddr.toString().equals("");

        } catch (Exception e) {
            return false;
        }
    }

    private void CheckInternetConnection() {
        if (!isNetworkConnected()) {
            InternetIsAvailable = false;
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                InternetIsAvailable = isInternetAvailable();
            }
        }).start();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_READ_PHONE_STATE: {
                for (int result : grantResults) {
                    if (result != PackageManager.PERMISSION_GRANTED) {
                        BlockCallsSwitch.setChecked(false);
                        BlockCalls = false;
                        Toast.makeText(this, getApplicationContext().getString(R.string.need_internet_connection), Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                DoNotShowDialog();
            }
        }
    }

    private void ReloadAppsList() {
        pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        String RowSelectedApps = pref.getString("PackagesList", "");
        SelectedAppsPackages = RowSelectedApps.split(",");
    }

    private void InflateAppsList() {
        if (VpnStarted) {
            vpnButton.performClick();
        }
        ReloadAppsList();
        final LinearLayout castsContainer = findViewById(R.id.casts_container);
        castsContainer.removeAllViews();
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        LinearLayout clickableColumn;
        int size = SelectedAppsPackages.length;


        if (!SelectedAppsPackages[0].equals("")) {
            ((TextView) findViewById(R.id.NoAppsSelectedTextView)).setText("");
        } else {
            ((TextView) findViewById(R.id.NoAppsSelectedTextView)).setText(getApplicationContext().getString(R.string.no_apps_selected));
            return;
        }

        for (int i = 0; i < size; i++) {
            String CurrPackage = SelectedAppsPackages[i];
            // create dynamic LinearLayout and set Image on it.
            if (!CurrPackage.equals("")) {
                clickableColumn = (LinearLayout) inflater.inflate(R.layout.clickable_column, null);
                ImageView thumbnailImage = clickableColumn.findViewById(R.id.thumbnail_image);

                try {
                    Drawable icon = getPackageManager().getApplicationIcon(CurrPackage);
                    thumbnailImage.setImageDrawable(icon);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }

                final ImageView RemoveApp = clickableColumn.findViewById(R.id.RemoveApp);
                RemoveApp.setTag(i);
                castsContainer.addView(clickableColumn);


                RemoveApp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int position = (int) view.getTag();
                        String ToRemove = (pref.getString("PackagesList", "").split(",")[position]);
                        String NewString = removeFromStringArr(pref.getString("PackagesList", ""), ToRemove);
                        editor.putString("PackagesList", NewString);
                        editor.apply();
                        castsContainer.getChildAt(position).setVisibility(View.VISIBLE);
                        castsContainer.getChildAt(position).startAnimation(FadeOut);
                        if (VpnStarted) {
                            vpnButton.performClick();
                        }
                        InflateAppsList();
                    }
                });
            }
        }

        clickableColumn = (LinearLayout) inflater.inflate(R.layout.clickable_column, null);
        ImageView thumbnailImage = clickableColumn.findViewById(R.id.thumbnail_image);
        thumbnailImage.setImageDrawable(getDrawable(R.drawable.ic_round_add_circle_outline_24px));
        thumbnailImage.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

        final ImageView RemoveApp = clickableColumn.findViewById(R.id.RemoveApp);
        final ImageView WhiteImage = clickableColumn.findViewById(R.id.WhiteImageView);
        Drawable transparentDrawable = new ColorDrawable(Color.TRANSPARENT);
        RemoveApp.setImageDrawable(transparentDrawable);
        WhiteImage.setImageDrawable(transparentDrawable);
        thumbnailImage.setTag("AddImageButton");
        castsContainer.addView(clickableColumn);

        thumbnailImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getTag().toString().equals("AddImageButton")) {
                    Intent AppList = new Intent(getApplicationContext(), SelectAppsActivity.class);
                    AppList.putExtra("AddSelected", true);
                    startActivity(AppList);
                }
            }
        });
    }

    private void startVPN() {
        LocalBroadcastManager.getInstance(this).registerReceiver(vpnStateReceiver, new IntentFilter(LocalVPNService.BROADCAST_VPN_STATE));
        Intent vpnIntent = VpnService.prepare(this);
        if (vpnIntent != null)
            startActivityForResult(vpnIntent, VPN_REQUEST_CODE);
        else
            onActivityResult(VPN_REQUEST_CODE, RESULT_OK, null);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == VPN_REQUEST_CODE && resultCode == RESULT_OK) {
            waitingForVPNStart = true;
            startService(new Intent(this, LocalVPNService.class));
            VpnStarted = true;
            enableButton(true);
        } else if (requestCode == VPN_REQUEST_CODE && resultCode != RESULT_OK) {
            waitingForVPNStart = false;
            VpnStarted = false;
            Toast.makeText(getApplicationContext(), getApplicationContext().getString(R.string.allow_vpn), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        VpnStarted = LocalVPNService.isRunning();
        enableButton(VpnStarted || waitingForVPNStart);
    }

    private void enableButton(boolean IsVpnEnabled) {
        if (IsVpnEnabled) {
            vpnButton.setText(getApplicationContext().getString(R.string.stop_button));
            pulsator.start();
            editor.putBoolean("BlockCalls", ((Switch) findViewById(R.id.BlockCallsSwitch)).isChecked());
            editor.apply();
        } else {
            pulsator.stop();
            vpnButton.setText(getApplicationContext().getString(R.string.start_button));
            editor.putBoolean("BlockCalls", false);
            editor.apply();
        }
    }

    public void DoNotShowDialog() {
        if (!VpnStarted) {
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(MainActivity.this);
            View mView = getLayoutInflater().inflate(R.layout.dialog_content, null);
            CheckBox mCheckBox = mView.findViewById(R.id.checkBox);
            mBuilder.setTitle(getApplicationContext().getString(R.string.block_calls_title));
            mBuilder.setMessage(getApplicationContext().getString(R.string.blocking_start_after_button));
            mBuilder.setView(mView);
            mBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });

            AlertDialog mDialog = mBuilder.create();
            mDialog.show();
            mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (compoundButton.isChecked()) {
                        storeDialogStatus(true);
                    } else {
                        storeDialogStatus(false);
                    }
                }
            });

            if (getDialogStatus()) {
                mDialog.hide();
            } else {
                mDialog.show();
            }
        }
    }

    private void storeDialogStatus(boolean isChecked) {
        SharedPreferences mSharedPreferences = getSharedPreferences("CheckItem", MODE_PRIVATE);
        SharedPreferences.Editor mEditor = mSharedPreferences.edit();
        mEditor.putBoolean("item", isChecked);
        mEditor.apply();
    }

    private boolean getDialogStatus() {
        SharedPreferences mSharedPreferences = getSharedPreferences("CheckItem", MODE_PRIVATE);
        return mSharedPreferences.getBoolean("item", false);
    }
}
