package com.yossefdouieb.WiFocus;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    public final List<Model> mModelList;
    private final PackageManager packageManager;
    private final ArrayList<String> data;
    private final Context context;

    public RecyclerViewAdapter(Context context, ArrayList<String> data) {
        packageManager = context.getPackageManager();
        this.data = data;
        this.context = context;
        mModelList = new ArrayList<>();
        for (String curr : data) {
            mModelList.add(new Model(curr));
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Model model = mModelList.get(position);
        try {
            holder.mTitle.setText(packageManager.getApplicationLabel(packageManager.getApplicationInfo(data.get(position), PackageManager.GET_META_DATA)));
            Drawable icon = packageManager.getApplicationIcon(data.get(position));
            holder.mImageView.setImageDrawable(icon);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                model.setSelected(!model.isSelected());
                GradientDrawable gd = new GradientDrawable();
                gd.setCornerRadius(180);
                if (model.isSelected()) {
                    gd.setColor(new ColorDrawable(ContextCompat.getColor(context, R.color.colorPrimary)).getColor());
                } else {
                    gd.setColor(Color.WHITE);
                }
                holder.view.setBackground(gd);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mModelList == null ? 0 : mModelList.size();
    }

    public void removeItem(int position) {
        data.remove(position);
        mModelList.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(String item, int position) {
        data.add(position, item);
        mModelList.add(position, new Model(item));
        notifyItemInserted(position);
    }

    public ArrayList<String> getData() {
        return data;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        final RelativeLayout relativeLayout;
        private final TextView mTitle;
        private final ImageView mImageView;
        private final View view;

        MyViewHolder(View itemView) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.iconImageView);
            view = itemView;
            mTitle = itemView.findViewById(R.id.txtTitle);
            relativeLayout = itemView.findViewById(R.id.relativeLayout);
        }
    }

    public class Model {

        private final String text;
        private boolean isSelected = false;

        Model(String text) {
            this.text = text;
        }

        public String getText() {
            return text;
        }

        public boolean isSelected() {
            return isSelected;
        }

        void setSelected(boolean selected) {
            isSelected = selected;
        }
    }
}


